locals {
    secret_env_vars = var.secrets
}

variable "secrets" {
    type = map(any)
    default = {
        "SECRET_KEY_1" = {
            secret = "projects/test"
            version = "latest"
        }
    }
    description = "object for storing secrets"
    validation {
        condition = (var.secrets != null) ? alltrue(
        [for key, value in var.secrets : key != null]
        ): true
        error_message = "Secret key requires name (string)"
    }
    validation {
        condition = (var.secrets != null) ? alltrue(
        [for secret in var.secrets : secret.version != null]
        ): true
        error_message = "Secret version requires name (string)"
    }
    validation {
        condition = (var.secrets != null) ? alltrue(
        [for secret in var.secrets : can(regex("^projects/", secret.secret))]
        ): true
        error_message = "Secrets.secret requires string to start with 'projects/'"
    }
}

output "secrets_var" {
    value = var.secrets
}

output "secrets_key_exists_validation" {
    value = (var.secrets != null) ? alltrue([for key, value in var.secrets: key != null]): true
}

output "secrets_version_exists_validation" {
    value = (var.secrets != null) ? alltrue([for secret in var.secrets : secret.version != null]) : true
}

output "secrets_secret_regex_validation" {
    value = (var.secrets != null) ? alltrue([for secret in var.secrets : can(regex("^projects/", secret.secret))]) : true
}
