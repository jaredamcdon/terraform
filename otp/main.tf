resource "random_string" "key_gen" {
    special = false
    numeric = false
    length = length(local.plaintext)
}

locals {
    alphabet = [
        "A", "B", "C", "D", "E", "F", "G",
        "H", "I", "J", "K", "L", "M", "N",
        "O", "P", "Q", "R", "S", "T", "U",
        "V", "W", "X", "Y", "Z", " "
    ]
    alphanum = {
        "A": 0,"B": 1,"C": 2,"D": 3, "E": 4,
        "F": 5,"G": 6,"H": 7, "I": 8, "J": 9,
        "K": 10,"L": 11,"M": 12, "N": 13,
        "O": 14,"P": 15,"Q": 16, "R": 17,
        "S": 18,"T": 19,"U": 20, "V": 21,
        "W": 22,"X": 23,"Y": 24, "Z": 25,
        " ": 26
    }
    pkc = jsondecode(file("pkc.json"))

    plaintext = upper(local.pkc.inputs.p)
    cipher = replace("${join(",", local.cipher_letter)}", ",", "")
    key = upper(random_string.key_gen.result)

    plaintext_letter = split("", local.plaintext)
    cipher_letter = [for i in range(length(local.plaintext_num)) : local.alphabet[((27 + local.plaintext_num[i] + local.key_num[i]) % 27)]]
    key_letter = split("", local.key)

    plaintext_num = [for letter in local.plaintext_letter : local.alphanum[letter] ]
    cipher_num = [for letter in local.cipher_letter : local.alphanum[letter] ]
    key_num = [for letter in local.key_letter : local.alphanum[letter] ]
}

output "Inputs" {
    value = "\tPlaintext: \t${local.plaintext}\n\tKey: \t\t${local.key}\n\tCipher:\t\t${local.cipher}"
}
